<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 18/6/27
 * Time: 下午3:37
 */

namespace App\Handlers;

use App\Models\Banner;

class BannerHandler
{
    public function frontend()
    {
        return app(Banner::class)->ordered()->recent('asc')->active()->get();
    }
}