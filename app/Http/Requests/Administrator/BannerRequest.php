<?php

namespace App\Http\Requests\Administrator;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

class BannerRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
//          create
            case 'POST': {
                return [
                    'b_url_id' => 'required|max:191',
                    'p_url_id' => 'required|max:191',
                    'url' => 'nullable|max:191',
                    'description' => 'nullable|min:1|max:65535',
                ];
            }
//          update
            case 'PUT':
            case 'PATCH': {
                return [
                    'b_url_id' => 'required|max:191',
                    'p_url_id' => 'required|max:191',
                    'url' => 'nullable|url|max:191',
                    'description' => 'nullable|min:1|max:65535',
                    'order' => 'nullable|integer',
                    'status' => 'nullable|'.Rule::in(['0','1']),
                    'open' => 'nullable|'.Rule::in(['0','1'])
                ];
            }
        }
    }
}
