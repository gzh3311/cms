<?php
/**
 * LaraCMS - CMS based on laravel
 *
 * @category  LaraCMS
 * @package   Laravel
 * @author    Wanglelecc <wanglelecc@gmail.com>
 * @date      2018/06/06 09:08:00
 * @copyright Copyright 2018 LaraCMS
 * @license   https://opensource.org/licenses/MIT
 * @github    https://github.com/wanglelecc/laracms
 * @link      https://www.laracms.cn
 * @version   Release 1.0
 */

namespace App\Observers;

use App\Models\Banner;

// creating, created, updating, updated, saving,
// saved,  deleting, deleted, restoring, restored

/**
 * banner观察者
 *
 * Class CategoryObserver
 * @package App\Observers
 */
class BannerObserver
{
    public function creating(Banner $banner)
    {
        $banner->order = $banner->order ?? 999;
        $banner->open = $banner->open ?? 1;
        $banner->status = $banner->status ?? 1;
    }

    public function updating(Banner $banner)
    {

    }

    public function saving(Banner $banner){

    }

    public function deleting(Banner $banner){
        
    }


    public function updated(Banner $banner){
       
    }
}