<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Banner;

class BannerPolicy
{

    public function index(User $user, Banner $banner)
    {
        return $user->can('manage_banner');
    }

    public function create(User $user, Banner $banner)
    {
        return $user->can('manage_banner');
    }

    public function update(User $user, Banner $banner)
    {
        return $user->can('manage_banner');
    }

    public function destroy(User $user, Banner $banner)
    {
        return $user->can('manage_banner');
    }
}
