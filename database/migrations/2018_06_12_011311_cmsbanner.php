<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cmsbanner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('banners', function ($table) {
            $table->bigIncrements('id');
            $table->string('description')->comment('图片文字描述')->nullable();
            $table->string('url')->comment('外链')->nullable();
            $table->string('b_url_id')->comment('banner图');
            $table->string('p_url_id')->comment('产品图');
            $table->integer('order')->comment('排序')->default(0);
            $table->tinyInteger('open')->comment('打开状态')->default(0);
            $table->tinyInteger('status')->comment('状态')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('banners');
    }
}
