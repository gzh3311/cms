@extends('frontend.default.new.app')

@section('title', $title = $category->name )

@section('styles')
    <style>
        .page {
            margin-top: 20px;
        }
    </style>
@endsection

@section('breadcrumb')
@endsection


@section('content')

    <section id="portfolio">
        <div class="container">
            <div class="center">
                <h2>{{ $title }}</h2>

                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                    incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
            </div>

            <div class="row">
                <div class="portfolio-items">
                    @if($articles->count())
                        @foreach($articles as $index => $article)
                            <div class="portfolio-item apps col-xs-12 col-sm-4 col-md-3">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive" src="{{ $article->getThumb() }}" alt="">

                                    <div class="overlay">
                                        <div class="recent-work-inner">
                                            <h3><a href="#">{{ $article->title }}</a></h3>

                                            <p>{{ $article->subtitle }}</p>
                                            <a class="preview" href="{{ $article->getLink($navigation,$category->id) }}"><i class="fa fa-eye"></i> View</a>
                                        </div>
                                    </div>
                                </div>
                            </div><!--/.portfolio-item-->
                        @endforeach
                    @endif

                </div>
            </div>
            <div class="text-center page">
                {{ $articles->links() }}
            </div>
        </div>
    </section><!--/#portfolio-item-->

@endsection
