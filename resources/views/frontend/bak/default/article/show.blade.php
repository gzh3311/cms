@extends('frontend.default.new.app')

@section('title', $title = $article->title )

@section('styles')
    <style>
        .detail-about {
            position: relative;
            line-height: 20px;
            padding: 15px 15px 15px 15px;
            font-size: 13px;
            background-color: #f8f8f8;
            color: #999;
            text-indent: 2em;
        }
    </style>
@endsection

@section('breadcrumb')
@endsection


@section('content')

    <section class="pricing-page">
        <div class="container">
            <div class="center">  
                <h2>{{$article->title}}</h2>
                <div class="detail-about">{{ $article->subtitle }}</div>
            </div>  
            <div class="text-center">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        {!! $article->content !!}
                    </div>
                </div>
            </div><!--/pricing-area-->
        </div><!--/container-->
    </section><!--/pricing-page-->
           
@endsection