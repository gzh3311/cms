@extends('frontend.default.new.app')

@section('title', $title = $category->name )

@section('styles')
@endsection

@section('breadcrumb')
@endsection


@section('content')

    @php
    $currentBrothersAndChildNavigation = frontend_current_brother_and_child_navigation('desktop',true);
    @endphp

    <section id="blog" class="container">
        <div class="center">
            <h2>{{ $title }}</h2>

            <p class="lead">Pellentesque habitant morbi tristique senectus et netus et malesuada</p>
        </div>

        <div class="blog">
            <div class="row">
                <div class="col-md-8">
                    @if($articles->count())
                        @foreach($articles as $index => $article)
                            <div class="blog-item">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-2 text-center">
                                        <div class="entry-meta">
                                            <span id="publish_date">{{ date('m月d日',strtotime($article->created_at)) }}</span>
                                            <span><i class="fa fa-user"></i> <a href="#">{{ $article->author }}</a></span>
                                        <span><i class="fa fa-eye"></i> <a href="#">{{ $article->views }}
                                                views</a></span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-10 blog-content">
                                        <a href="#"><img class="img-responsive img-blog" src="{{ $article->getThumb() }}"
                                                         width="100%" alt=""/></a>

                                        <h2><a href="{{$article->getLink($navigation,$category->id)}}">{{ $article->title }}</a></h2>

                                        <h3>{{ $article->description }}</h3>
                                        <a class="btn btn-primary readmore" href="{{$article->getLink($navigation,$category->id)}}">Read More <i
                                                    class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div><!--/.blog-item-->
                        @endforeach

                        {{--<ul class="pagination pagination-lg">--}}
                            {{--<li><a href="#"><i class="fa fa-long-arrow-left"></i>Previous Page</a></li>--}}
                            {{--<li class="active"><a href="#">1</a></li>--}}
                            {{--<li><a href="#">2</a></li>--}}
                            {{--<li><a href="#">3</a></li>--}}
                            {{--<li><a href="#">4</a></li>--}}
                            {{--<li><a href="#">5</a></li>--}}
                            {{--<li><a href="#">Next Page<i class="fa fa-long-arrow-right"></i></a></li>--}}
                        {{--</ul><!--/.pagination-->--}}
                        <div class="col-xs-12 col-sm-10 col-sm-offset-2">
                            {{ $articles->links() }}
                        </div>
                    @endif
                </div>
                <!--/.col-md-8-->
                <aside class="col-md-4">
                    <div class="widget search">
                        <form role="form">
                            <input type="text" class="form-control search_box" autocomplete="off"
                                   placeholder="Search Here">
                        </form>
                    </div>
                    <!--/.search-->

                    @if($currentBrothersAndChildNavigation)
                    <div class="widget categories">
                        <h3>Categories</h3>

                        <div class="row">
                            <div class="col-sm-12">
                                <ul class="blog_category">
                                    @foreach($currentBrothersAndChildNavigation as $navigation)
                                        <li><a target="{{ $navigation->target }}" href="{{$navigation->link}}" rel="nofollow" class="fly-category @if(request('navigation',0)==$navigation->id)fly-category-this @endif">{{$navigation->title}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!--/.categories-->
                </aside>
            </div>
            <!--/.row-->
        </div>
    </section><!--/#blog-->

@endsection