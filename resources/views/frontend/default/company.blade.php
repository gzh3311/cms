@extends('frontend.default.new.app')

@section('title', $title = '关于我们' )

@section('styles')
@endsection

@section('breadcrumb')
@endsection

@section('bodyclass','homepage')

@section('content')

    <section id="company">

    </section>
    <div class="layui-container">
          <div class="layui-row layui-col-space15">
            <div class="layui-col-md12 content detail">
                <div class="fly-panel detail-box">
                    <h1>{{$title}}</h1>
                    <div class="detail-body layui-text photos">
                        {!! config("system.common.company.content") !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection