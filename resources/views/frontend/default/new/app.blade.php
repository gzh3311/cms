<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', config('system.common.basic.name', 'LaraCMS')) - {{ config('app.name', 'LaraCMS')  }}</title>
    <meta name="description" content="@yield('description', config('system.common.basic.description',''))">
    <meta name="Keywords" content="@yield('keywords', config('system.common.basic.keywords',''))">
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token()
        ]) !!};
    </script>
    <!-- core CSS -->
    <link href="{{ asset('mednove/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('mednove/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('mednove/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('mednove/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('mednove/css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('mednove/css/responsive.css') }}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="{{ asset('mednove/js/html5shiv.js') }}"></script>
    <script src="{{ asset('mednove/js/respond.min.js') }}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{ asset('mednove/images/ico/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('mednove/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('mednove/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('mednove/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('mednove/images/ico/apple-touch-icon-57-precomposed.png') }}">
    @yield('styles')
</head>
<body  class="@yield('bodyclass')">

{{--<div id="app" class="{{ route_class() }}-page">--}}

    @include('frontend.default.new._header')

    {{--<div class="{{ route_class() }}-content">--}}
        {{--<div class="">--}}
            @yield('tab')
            {{--@include('frontend.default.layouts._breadcrumb')--}}
            @yield('content')
        {{--</div>--}}
    {{--</div>--}}

    @include('frontend.default.new._footer')
{{--</div>--}}

<!-- Scripts -->
<script src="{{ asset('mednove/js/jquery.js') }}"></script>
<script src="{{ asset('mednove/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('mednove/js/jquery.prettyPhoto.js') }}"></script>
<script src="{{ asset('mednove/js/jquery.isotope.min.js') }}"></script>
<script src="{{ asset('mednove/js/main.js') }}"></script>
<script src="{{ asset('mednove/js/wow.min.js') }}"></script>


@yield('scripts')

@include('frontend.default.new._statistics')
</body>
</html>
