@extends('frontend.default.new.app')

@section('title', $title = $page->title )
@section('description', $page->description)
@section('keywords', $page->keywords)

@section('styles')
    <style>
        .detail-about {
            position: relative;
            line-height: 20px;
            padding: 15px 15px 15px 15px;
            font-size: 13px;
            background-color: #f8f8f8;
            color: #999;
            text-indent: 2em;
        }
    </style>
@endsection

@section('breadcrumb')
@endsection


@section('content')

    <section id="blog" class="container">
        <div class="center">
            <h2>{{$page->title}}</h2>
            <p class="lead">{{ $page->subtitle }}</p>
        </div>

        <div class="blog">
            <div class="row">

                <div class="col-md-12">
                    <div class="blog-item">
                        <div class="row">
                            <div class="col-xs-12 col-sm-2 text-center">
                                <div class="entry-meta">
                                    <span id="publish_date">{{ $page->getDate()}}</span>
                                    <span><i class="fa fa-user"></i> <a href="#">{{$page->getAuthor()}}</a></span>
                                    <span><i class="fa fa-eye"></i><a href="#">{{ $page->views }}</a></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-10 blog-content">
                                @if($page->description && false)
                                    {{ $page->description }}
                                @endif
                                {!! $page->content !!}
                            </div>
                        </div>
                    </div><!--/.blog-item-->
                </div><!--/.col-md-8-->

            </div><!--/.row-->

        </div><!--/.blog-->

    </section><!--/#blog-->

@endsection